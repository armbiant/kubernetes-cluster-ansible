FROM python:3.11.1-alpine3.17@sha256:9f955672f82a7e7138bcb64bb8352fb0a3025533b8be90a789be5a32526497ca

# renovate: datasource=repology depName=alpine_3_17/openssh versioning=loose
ARG OPENSSH_VER='9.1_p1-r2'
# renovate: datasource=repology depName=alpine_3_17/git versioning=loose
ARG GIT_VER='2.38.3-r1'
# renovate: datasource=repology depName=alpine_3_17/sshpass versioning=loose
ARG SSHPASS_VER='1.09-r1'
# renovate: datasource=github-releases depName=helm/helm
ARG HELM_VER='v3.11.0'

COPY requirements.txt /requirements.txt
COPY bootstrap/requirements.yml /requirements.yml

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

RUN apk add --no-cache \
        "git=$GIT_VER" \
        "openssh=$OPENSSH_VER" \
        "sshpass=$SSHPASS_VER" \
 && wget -qO - "https://get.helm.sh/helm-${HELM_VER}-linux-amd64.tar.gz" | \
    tar xvzf - --strip-components 1 -C /usr/local/bin linux-amd64/helm \
 && pip install --no-cache-dir -r /requirements.txt \
 && ansible-galaxy install --role-file /requirements.yml
