# GitLab Cleanups

## Container Registry

From the GitLab repository → `Settings` → `Package and registry`
→ `Clean up image tags` → `Set cleanup rules`:

* [x] `Enabled`
* `Run cleanup`: `Every day`
* `Keep these tags`:
  * `Keep the most recent`: 5
  * `Keep tags matching`: `main`
* `Remove these tags`:
  * `Remove tags older than`: `90 days`
  * `Remove tags matching`: `.*`
