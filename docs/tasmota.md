# Tasmota

## Configure timer (TV)

| id | Output | Action | Enable | Repeat | Time  | Days                        |
|----|--------|--------|--------|--------|-------|-----------------------------|
| 1  | 1      | Off    | ✅      | ✅      | 23.30 | Sun,Mon,Tue,Wed,Thu,Fri,Sat |
| 2  | 1      | On     | ✅      | ✅      | 19.00 | Mon,Tue,Wed,Thu,Fri         |
| 3  | 1      | On     | ✅      | ✅      | 08.00 | Sun,Sat                     |

## Factory settings (Delock 11827)

Firmware: <http://www.delock.de/download/firmware/11827-de.bin>

To downgrade install `tasmota-lite.bin.gz` from <http://ota.tasmota.com/tasmota/release/>

### MQTT-Einstellungen

| Key                  | Value             |
|----------------------|-------------------|
| Host                 |                   |
| Port                 | 1883              |
| Client (DVES_D742EB) | DVES\_%06X        |
| Benutzer (DVES_USER) | DVES_USER         |
| Passwort             |                   |
| topic %topic%        | delock            |
| full topic           | %prefix%/%topic%/ |

### Domoticz-Parameter

| Key                        | Value |
|----------------------------|-------|
| Idx 1                      | 0     |
| Key idx 1                  | 0     |
| Sensor idx 1 Temp          | 0     |
| Sensor idx 2 Temp,Hum      | 0     |
| Sensor idx 3 Temp,Hum,Baro | 0     |
| Sensor idx 4 Power,Energy  | 0     |
| Sensor idx 5 Illuminance   | 0     |
| Sensor idx 6 Count/PM1     | 0     |
| Sensor idx 7 Voltage/PM2.5 | 0     |
| Sensor idx 8 Current/PM10  | 0     |
| Sensor idx 9 AirQuality    | 0     |
| Sensor idx 10 P1SmartMeter | 0     |
| Sensor idx 11 Rollo        | 0     |
| Update Zeitplan            | 0     |

### Logging-Einstellungen

| Key               | Value   |
|-------------------|---------|
| Seriell-Log Level | 2 Info  |
| Web-Log Level     | 2 Info  |
| Mqtt-Log Level    | 0 keine |
| Sys-Log Level     | 0 keine |
| Sys-Log Host      |         |
| Sys-Log Port      | 514     |
| Telemetrieperiode | 300     |

### Sonstige Einstellungen

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD013 -->

| Key                         | Value                                                                                     |
|-----------------------------|-------------------------------------------------------------------------------------------|
| Vorlage                     | {"NAME":"Delock 11827","GPIO":\[0,0,0,17,133,132,0,0,131,158,21,0,0\],"FLAG":0,"BASE":53} |
| Aktivieren                  | :white_check_mark:                                                                        |
| Passwort für Web Oberfläche | :x:                                                                                       |
| MQTT aktivieren             | :white_check_mark:                                                                        |
| Name \[friendly name\] 1    | WLAN-Switch                                                                               |
| Emulation                   | Belkin WeMo                                                                               |

<!-- markdownlint-enable MD013 -->
<!-- markdownlint-restore -->
