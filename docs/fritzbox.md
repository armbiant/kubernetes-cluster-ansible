# FRITZ!Box 7530 AX

## Internet → Account information

### Internet Connection

* Internet service provider: Telekom
* Select your type of connection: Telekom DSL-Anschluss: EMPFOHLEN

### IPv6

* [ ] IPv6 support enabled

### LIST

* [ ] LISP support enabled

### Provider Services

* [ ] Allow automatic configuration by the internet provider

### AVM Services

* [ ] FRITZ!Box searches for updates periodically
* [ ] Diagnostics data

### DNS Servers

* DNSv4 Servers:
  * Use other DNSv4 servers
    * Preferred DNSv4 server: 192.168.178.201
    * Alternative DNSv4 server: 1.1.1.1
* DNS over TLS (DoT):
  * [ ] Encrypted name resolution in the internet (DNS over TLS)

## Home Network

### Network → Network Settings

* Operating Mode in the Home Network: Internet router

Access Settings in the Home Network:

* [x] Allow access for applications
* [x] Transmit status information over UPnP

IP Addresses:

* IPv4 address: 192.168.178.1
* Subnet mask: 255.255.255.0
* [x] Enable DHCP server
  * DHCP server assigns IPv4 addresses:
    * from: 192.168.178.20
    * to: 192.168.178.199
    * Valid for 10 days
  * Local DNS server: 192.168.178.201

### USB/Storage

* [ ] Storage (NAS) function of FRITZ!Box 7530 AX enabled
* [ ] USB remote connection enabled

### Media Server

* [ ] Media Server enabled

## Wi-Fi

### Security

* [x] WPA encryption (highest security)
* [ ] WPS enabled

## System

### Buttons and LEDs

* LED Brightness: weak
