# Renovate

The configuration file for [Renovate](https://renovatebot.com) is `.gitlab/renovate.json`

## Requirements

### GitLab Token

Required to interact with GitLab: create merge requests, create issue dashboard ...

Login to `gitlab.com` → top right corner → `Edit Profile` → `Access Token`:

* `Token name`: `RENOVATE`
* `Expiration date`
* Select scopes:
  * `api`
  * `read_user`
  * `write_repository`

### GitHub Token

Required to read release notes and GitHub releases.

Login to `github.com`→ top right corner → `Settings` → `Developer settings`
→ `Personal access tokens` → `Fine-grained tokens`:

* `Token name`: `RENOVATE`
* `Expiration`
* `Repository access`: `All repositories`
* `Permissions`: None

### CI Variables

From the GitLab repository → `Settings` → `CI/CD` → `Variables` → `Add variable`:

1. GitLab
    * `Key`: `RENOVATE_TOKEN`
    * `Value`: GitLab token above
    * [ ] Protect variable
    * [x] Mask variable

2. GitHub

    * `Key`: `GITHUB_COM_TOKEN`
    * `Value`: GitHub token above
    * [ ] Protect variable
    * [x] Mask variable
