#!/usr/bin/env bash
set -euo pipefail

echo "[INFO] Chart: $CHART"
helm dependency build "$CHART"
helm package "$CHART"

tgz=$(echo *.tgz)
echo "[INFO] $tgz"

if curl --silent --fail --head --output /dev/null "${HELM_REPO}/charts/${tgz}"; then
    echo "[ERROR] $tgz already present"
    exit 1
fi

echo "[INFO] $tgz - uploading"
curl \
    --request POST \
    --user "gitlab-ci-token:$CI_JOB_TOKEN" \
    --form "chart=@$tgz" \
    "$HELM_API"

echo "[INFO] Add helm repository:"
echo "helm repo add mule $HELM_REPO"
