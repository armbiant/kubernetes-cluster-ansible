#!/usr/bin/env bash
set -xeuo pipefail

# Install
apk add --no-cache ansible-core podman yq

# Install - kind
curl \
    --location \
    --output /usr/local/bin/kind \
    "https://github.com/kubernetes-sigs/kind/releases/download/${KIND_VER}/kind-linux-amd64"
chmod +x /usr/local/bin/kind

# Volumes
df -h
mkdir -p /cache/influxdb
mkdir -p /cache/jellyfin-config
mkdir -p /cache/jellyfin-media
mkdir -p /cache/prometheus

# Start cluster
export KIND_EXPERIMENTAL_PROVIDER=podman
time kind create cluster \
    --config .gitlab/kind_config.yaml \
    --image "kindest/node:v1.25.3"
mkdir -p "${HOME}/.kube"
kind get kubeconfig >"${HOME}/.kube/config"

# Show
kubectl version
kubectl cluster-info
time kubectl wait nodes --for condition=Ready --all --timeout=60s
kubectl get nodes -o wide
kubectl get nodes -o custom-columns=NAME:.metadata.name,TAINTS:.spec.taints --no-headers
kubectl describe nodes | grep 'Name:\|  cpu\|  memory'
kubectl describe nodes | grep "Allocated resources" -A 9
kubectl get pods --all-namespaces -o wide
kubectl get services --all-namespaces -o wide

# System
ansible-vault decrypt bootstrap/group_vars/all/secrets.yaml
yq '.tls_public_key' bootstrap/group_vars/all/secrets.yaml >tls.crt
yq '.tls_private_key' bootstrap/group_vars/all/secrets.yaml >tls.key

kubectl create namespace system
kubectl config set-context --current --namespace system
kubectl create secret tls sealed-secrets-tls --cert tls.crt --key tls.key
kubectl label secret sealed-secrets-tls sealedsecrets.bitnami.com/sealed-secrets-key=active

helm dependency update charts/system
helm template system charts/system --include-crds | kubectl create --filename -

time kubectl wait pods system-influxdb2-0 --for condition=Ready --timeout=60s
# time kubectl wait pods --for condition=Ready --all --all-namespaces --timeout=60s
