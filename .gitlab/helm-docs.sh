#!/usr/bin/env bash
set -euo pipefail

podman run \
    --rm \
    --volume "${PWD}:/helm-docs" \
    --security-opt label=disable \
    docker.io/jnorwood/helm-docs:latest

podman run \
    --rm \
    --volume "${PWD}:/data" \
    --workdir "/data" \
    --security-opt label=disable \
    docker.io/library/node \
    bash -c '''
        npm install --global markdown-table-formatter
        markdown-table-formatter "**/*.md"
    '''
