#!/usr/bin/env python
"""Check adlist"""

from requests import get
from yaml import safe_load

firebog = get("https://v.firebog.net/hosts/lists.php?type=nocross", timeout=10).content.decode("utf8").split()

# Read existing
with open("charts/pihole/ci/mule-values.yaml", encoding="utf8") as obj:
    adlists = safe_load(obj)["adlists"]

FAIL = False
print("[INFO] urls from firebog")
for url in firebog:
    if url not in adlists:
        print(f"- {url}")
        FAIL = True

print("[INFO] url availability")
for url in adlists:
    if not get(url, timeout=10).content:
        print(f"- {url}")
        FAIL = True

if FAIL:
    raise Exception("Errors found")
