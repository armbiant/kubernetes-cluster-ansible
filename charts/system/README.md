# system

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD013 MD034 -->

![Version: 1.6.6](https://img.shields.io/badge/Version-1.6.6-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.6.6](https://img.shields.io/badge/AppVersion-1.6.6-informational?style=flat-square)

<!-- markdownlint-enable MD013 MD034 -->
<!-- markdownlint-restore -->

Install system

**Homepage:** <https://gitlab.com/yellowhat-labs/mule/-/tree/main/charts/system>

## Maintainers

| Name      | Email                   | Url |
|-----------|-------------------------|-----|
| yellowhat | <yellowhat@mailbox.org> |     |

## Source Code

* <https://metallb.universe.tf>
* <https://gitlab.com/yellowhat-labs/mule/-/tree/main/charts/system>

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD013 MD034 -->

## Requirements

| Repository                                         | Name                  | Version |
|----------------------------------------------------|-----------------------|---------|
| https://bitnami-labs.github.io/sealed-secrets      | sealed-secrets        | 2.7.3   |
| https://helm.influxdata.com                        | influxdb2             | 2.1.1   |
| https://metallb.github.io/metallb                  | metallb               | 0.13.7  |
| https://prometheus-community.github.io/helm-charts | kube-prometheus-stack | 44.4.0  |

## Values

| Key                                                                                                             | Type   | Default                                                                          | Description |
|-----------------------------------------------------------------------------------------------------------------|--------|----------------------------------------------------------------------------------|-------------|
| influxdb2.adminUser.bucket                                                                                      | string | `"default"`                                                                      |             |
| influxdb2.adminUser.organization                                                                                | string | `"influxdata"`                                                                   |             |
| influxdb2.adminUser.password                                                                                    | string | `"influxdb"`                                                                     |             |
| influxdb2.adminUser.token                                                                                       | string | `"0123456789ABCDEF"`                                                             |             |
| influxdb2.adminUser.user                                                                                        | string | `"admin"`                                                                        |             |
| influxdb2.env[0].name                                                                                           | string | `"INFLUXDB_REPORTING_DISABLED"`                                                  |             |
| influxdb2.env[0].value                                                                                          | string | `"true"`                                                                         |             |
| influxdb2.env[1].name                                                                                           | string | `"INFLUXD_REPORTING_DISABLED"`                                                   |             |
| influxdb2.env[1].value                                                                                          | string | `"true"`                                                                         |             |
| influxdb2.persistence.accessMode                                                                                | string | `"ReadWriteOnce"`                                                                |             |
| influxdb2.persistence.enabled                                                                                   | bool   | `true`                                                                           |             |
| influxdb2.persistence.size                                                                                      | string | `"50Gi"`                                                                         |             |
| influxdb2.persistence.storageClass                                                                              | string | `"influxdb"`                                                                     |             |
| kube-prometheus-stack.alertmanager.enabled                                                                      | bool   | `false`                                                                          |             |
| kube-prometheus-stack.grafana.additionalDataSources[0].access                                                   | string | `"proxy"`                                                                        |             |
| kube-prometheus-stack.grafana.additionalDataSources[0].basicAuth                                                | bool   | `false`                                                                          |             |
| kube-prometheus-stack.grafana.additionalDataSources[0].jsonData.defaultBucket                                   | string | `"default"`                                                                      |             |
| kube-prometheus-stack.grafana.additionalDataSources[0].jsonData.organization                                    | string | `"influxdata"`                                                                   |             |
| kube-prometheus-stack.grafana.additionalDataSources[0].jsonData.version                                         | string | `"Flux"`                                                                         |             |
| kube-prometheus-stack.grafana.additionalDataSources[0].name                                                     | string | `"influxdb"`                                                                     |             |
| kube-prometheus-stack.grafana.additionalDataSources[0].orgId                                                    | int    | `1`                                                                              |             |
| kube-prometheus-stack.grafana.additionalDataSources[0].secureJsonData.token                                     | string | `"0123456789ABCDEF"`                                                             |             |
| kube-prometheus-stack.grafana.additionalDataSources[0].type                                                     | string | `"influxdb"`                                                                     |             |
| kube-prometheus-stack.grafana.additionalDataSources[0].url                                                      | string | `"http://{{ printf \"%s-influxdb2.%s.svc\" .Release.Name .Release.Namespace }}"` |             |
| kube-prometheus-stack.grafana.admin.existingSecret                                                              | string | `"grafana-admin"`                                                                |             |
| kube-prometheus-stack.grafana.admin.passwordKey                                                                 | string | `"password"`                                                                     |             |
| kube-prometheus-stack.grafana.admin.userKey                                                                     | string | `"username"`                                                                     |             |
| kube-prometheus-stack.grafana.service.annotations."metallb.universe.tf/loadBalancerIPs"                         | string | `"192.168.178.200"`                                                              |             |
| kube-prometheus-stack.grafana.service.enabled                                                                   | bool   | `true`                                                                           |             |
| kube-prometheus-stack.grafana.service.type                                                                      | string | `"LoadBalancer"`                                                                 |             |
| kube-prometheus-stack.prometheus.prometheusSpec.additionalScrapeConfigs[0].job_name                             | string | `"tasmota"`                                                                      |             |
| kube-prometheus-stack.prometheus.prometheusSpec.additionalScrapeConfigs[0].metrics_path                         | string | `"/metrics"`                                                                     |             |
| kube-prometheus-stack.prometheus.prometheusSpec.additionalScrapeConfigs[0].static_configs[0].targets[0]         | string | `"192.168.178.100"`                                                              |             |
| kube-prometheus-stack.prometheus.prometheusSpec.additionalScrapeConfigs[0].static_configs[0].targets[1]         | string | `"192.168.178.101"`                                                              |             |
| kube-prometheus-stack.prometheus.prometheusSpec.retention                                                       | string | `"1y"`                                                                           |             |
| kube-prometheus-stack.prometheus.prometheusSpec.retentionSize                                                   | string | `"50GB"`                                                                         |             |
| kube-prometheus-stack.prometheus.prometheusSpec.securityContext.fsGroup                                         | int    | `2000`                                                                           |             |
| kube-prometheus-stack.prometheus.prometheusSpec.securityContext.fsGroupChangePolicy                             | string | `"OnRootMismatch"`                                                               |             |
| kube-prometheus-stack.prometheus.prometheusSpec.securityContext.runAsGroup                                      | int    | `2000`                                                                           |             |
| kube-prometheus-stack.prometheus.prometheusSpec.securityContext.runAsNonRoot                                    | bool   | `true`                                                                           |             |
| kube-prometheus-stack.prometheus.prometheusSpec.securityContext.runAsUser                                       | int    | `1000`                                                                           |             |
| kube-prometheus-stack.prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.accessModes[0]             | string | `"ReadWriteOnce"`                                                                |             |
| kube-prometheus-stack.prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.resources.requests.storage | string | `"100Gi"`                                                                        |             |
| kube-prometheus-stack.prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.storageClassName           | string | `"prometheus"`                                                                   |             |
| metallb.cdrs.enabled                                                                                            | bool   | `true`                                                                           |             |
| metallb.controller.resources.limits.cpu                                                                         | string | `"100m"`                                                                         |             |
| metallb.controller.resources.limits.memory                                                                      | string | `"100Mi"`                                                                        |             |
| metallb.controller.resources.requests.cpu                                                                       | string | `"100m"`                                                                         |             |
| metallb.controller.resources.requests.memory                                                                    | string | `"100Mi"`                                                                        |             |
| metallb.speaker.resources.limits.cpu                                                                            | string | `"100m"`                                                                         |             |
| metallb.speaker.resources.limits.memory                                                                         | string | `"100Mi"`                                                                        |             |
| metallb.speaker.resources.requests.cpu                                                                          | string | `"100m"`                                                                         |             |
| metallb.speaker.resources.requests.memory                                                                       | string | `"100Mi"`                                                                        |             |
| sealed-secrets.keyrenewperiod                                                                                   | string | `"0"`                                                                            |             |
| sealed-secrets.resources.limits.cpu                                                                             | string | `"150m"`                                                                         |             |
| sealed-secrets.resources.limits.memory                                                                          | string | `"256Mi"`                                                                        |             |
| sealed-secrets.resources.requests.cpu                                                                           | string | `"150m"`                                                                         |             |
| sealed-secrets.resources.requests.memory                                                                        | string | `"256Mi"`                                                                        |             |

<!-- markdownlint-enable MD013 MD034 -->
<!-- markdownlint-restore -->

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.0](https://github.com/norwoodj/helm-docs/releases/v1.11.0)
