# fritz-exporter

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD013 MD034 -->

![Version: 1.0.2](https://img.shields.io/badge/Version-1.0.2-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 2.2.1](https://img.shields.io/badge/AppVersion-2.2.1-informational?style=flat-square)

<!-- markdownlint-enable MD013 MD034 -->
<!-- markdownlint-restore -->

fritz-exporter Helm Chart

**Homepage:** <https://gitlab.com/yellowhat-labs/mule/-/tree/main/charts/fritz-exporter>

## Maintainers

| Name      | Email                   | Url |
|-----------|-------------------------|-----|
| yellowhat | <yellowhat@mailbox.org> |     |

## Source Code

* <https://github.com/pdreker/fritz_exporter>
* <https://gitlab.com/yellowhat-labs/mule/-/tree/main/charts/fritz-exporter>

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD013 MD034 -->

## Values

| Key                          | Type   | Default                                   | Description                                                                                |
|------------------------------|--------|-------------------------------------------|--------------------------------------------------------------------------------------------|
| affinity                     | object | `{}`                                      | Affinity rules                                                                             |
| config                       | object | `{}`                                      | Config file                                                                                |
| extraEnvVars                 | object | `{}`                                      | Extra environment variables                                                                |
| extraEnvVarsSecret           | object | `{}`                                      | Extra environment variables from secrets                                                   |
| fullnameOverride             | string | `""`                                      | Replaces the generated name                                                                |
| image.pullPolicy             | string | `"IfNotPresent"`                          | Pull policy                                                                                |
| image.repository             | string | `"docker.io/pdreker/fritz_exporter"`      | Repository to pull the image from                                                          |
| image.tag                    | string | `""`                                      | Image tag, if empty it will get it from the chart's appVersion                             |
| imagePullSecrets             | list   | `[]`                                      | Secret name for pulling images from private repository                                     |
| maxSurge                     | int    | `1`                                       | Maximum number of Pods that can be created over the desired number of Pods (RollingUpdate) |
| maxUnavailable               | int    | `0`                                       | Maximum number of Pods that can be unavailable during the update process (RollingUpdate)   |
| nameOverride                 | string | `""`                                      | Replaces the name of the chart in the Chart.yaml file                                      |
| nodeSelector                 | object | `{}`                                      | Node selection constraint                                                                  |
| podAnnotations               | object | `{}`                                      | Additional pod annotations                                                                 |
| podSecurityContext           | object | `{}`                                      | Privileges and access control settings for a Pod (all containers in a pod)                 |
| replicaCount                 | int    | `1`                                       | Number of replicas                                                                         |
| resources                    | object | `{}`                                      | CPU/MEM resources                                                                          |
| securityContext              | object | `{"runAsNonRoot":true,"runAsUser":65534}` | Privileges and access control settings for a container                                     |
| service.annotations          | object | `{}`                                      |                                                                                            |
| service.port                 | int    | `9787`                                    |                                                                                            |
| service.portName             | string | `"metrics"`                               |                                                                                            |
| service.type                 | string | `"ClusterIP"`                             |                                                                                            |
| serviceMonitor.enabled       | bool   | `true`                                    |                                                                                            |
| serviceMonitor.interval      | string | `"60s"`                                   |                                                                                            |
| serviceMonitor.labels        | object | `{}`                                      |                                                                                            |
| serviceMonitor.namespace     | string | `""`                                      |                                                                                            |
| serviceMonitor.scrapeTimeout | string | `"30s"`                                   |                                                                                            |
| strategyType                 | string | `"RollingUpdate"`                         | Deployment strategy                                                                        |
| tolerations                  | list   | `[]`                                      | Toleration for taints                                                                      |

<!-- markdownlint-enable MD013 MD034 -->
<!-- markdownlint-restore -->

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.0](https://github.com/norwoodj/helm-docs/releases/v1.11.0)
