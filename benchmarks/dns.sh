#!/usr/bin/env bash
# Benchmark dns against multiple nameservers
# set -euo pipefail

dnf install -y dnsperf

curl \
    --location \
    https://raw.githubusercontent.com/opendns/public-domain-lists/master/opendns-top-domains.txt |
    awk '{print $0 " A"}' >domains

echo "| Server | Clients | Time | Max req/s | Sent | Lost | Avg | Min | Max | Std |" >results
echo "| - | - | - | - | - | - | - | - | - | - |" >>results
for ip in "1.1.1.1" "8.8.8.8" "9.9.9.9" "208.67.222.222" "192.168.178.201"; do
    echo "[INFO] dnsperf $ip"
    dnsperf \
        -c "$NUM_CLIENTS" \
        -l "$TIME_LIMIT" \
        -Q "$MAX_QPS" \
        -s "$ip" \
        -d domains |
        grep -v "\[Timeout\]" |
        tee output
    echo -n "| $ip | $NUM_CLIENTS | $TIME_LIMIT | $MAX_QPS |" >>results
    cat output | grep "Queries sent" | awk '{printf " %s |", $3}' >>results
    cat output | grep "Queries lost" | awk '{printf " %s |", $3}' >>results
    cat output | grep "Average Latency" | awk '{printf " %5.1f | %5.1f | %5.1f |", $4*1000, $6*1000, $8*1000}' >>results
    cat output | grep "Latency StdDev" | awk '{printf " %5.1f |\n", $4*1000}' >>results
    echo "[INFO] Results:"
    cat results
done
