import http from 'k6/http'
import { check, fail } from 'k6'

export const options = {
  vus: 10,
  duration: '60s',
  thresholds: {
    checks: [
      'rate >= 1.0' // %, all checks must pass
    ],
    http_req_failed: [
      'rate <= 0.0' // %, no request failure
    ]
  }
}

export default function () {
  const response = http.get('http://192.168.178.202/web/index.html')

  const checkOutput = check(
    response,
    {
      'Response is 200': (r) => r.status === 200,
      'body size': (r) => r.body.length === 7442
    }
  )

  if (!checkOutput) {
    console.log(response.status)
    console.log(response.body.length)
    fail('Error: unexpected response')
  }
}
