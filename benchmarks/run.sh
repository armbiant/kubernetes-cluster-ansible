#!/usr/bin/env bash
# Benchmark deployments
set -euo pipefail

git clone https://gitlab.com/yellowhat/mule --depth 1
cd mule/benchmarks

for script in k6*.js; do
    log_file="${script}.log"
    podman run \
        --interactive \
        --rm \
        docker.io/grafana/k6:latest run - <"$script" | tee "$log_file"
    echo "$script" >>output.res
    grep "http_req_duration" "$log_file" >>output.res
done

podman run \
    --interactive \
    --rm \
    --env NUM_CLIENTS=10 \
    --env TIME_LIMIT=60 \
    --env MAX_QPS=1000 \
    registry.fedoraproject.org/fedora:37 bash <dns.sh
