#!/usr/bin/env bash
# alias

export EDITOR=nvim

# Expand aliases in shell script
shopt -s expand_aliases

alias \
    ..="cd .." \
    l=" ls -l --human-readable --color --group-directories-first" \
    ls="ls -l --human-readable --color --group-directories-first" \
    ll="ls -l --human-readable --color --group-directories-first" \
    la="ls -l --human-readable --color --group-directories-first --almost-all" \
    lt="ls -l --human-readable --color -t" \
    cp="cp --verbose" \
    mv="mv --verbose --context" \
    rm="rm --verbose" \
    df="df --human-readable" \
    du="du --human-readable" \
    v="\$EDITOR" \
    vi="\$EDITOR" \
    vim="\$EDITOR" \
    h="htop" \
    ww="watch -n 1"

# kubectl
# shellcheck disable=SC1090
source <(kubectl completion bash)
alias k="kubectl"
complete -F __start_kubectl k
declare -a aliases
aliases=(
    kd="kubectl describe"
    kdd="kubectl describe deployments"
    kde="kubectl describe endpoints"
    kdi="kubectl describe ingress"
    kdn="kubectl describe namespaces"
    kdp="kubectl describe pods"
    kds="kubectl describe services"
    kei="kubectl exec -it"
    kg="kubectl get"
    kgd="kubectl get deployments"
    kge="kubectl get endpoints"
    kgi="kubectl get ingress"
    kgn="kubectl get namespaces"
    kgp="kubectl get pods"
    kgs="kubectl get services"
    kl="kubectl logs"
    kn="kubectl config set-context --current --namespace"
    krm="kubectl delete"
    ku="kubectl describe nodes | grep 'Name:\|  cpu\|  memory'"
)
for al in "${aliases[@]}"; do
    alias "$al"
    complete -F _complete_alias "${al%=*}"
done
