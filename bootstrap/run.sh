#!/usr/bin/env bash
# Bootstrap the Kubernetes cluster
set -euo pipefail

TARGET_IP="192.168.178.31"

podman run \
    --interactive \
    --tty \
    --rm \
    --add-host "mule:${TARGET_IP}" \
    --env ANSIBLE_HOST_KEY_CHECKING=false \
    --pull newer \
    registry.gitlab.com/yellowhat-labs/mule:main sh -c '''
git clone https://gitlab.com/yellowhat-labs/mule --depth 1
cd mule/bootstrap

ansible-playbook \
    --user k8s \
    --ask-pass \
    --ask-become-pass \
    --ask-vault-pass \
    --inventory 192.168.178.31, \
    install.yaml
'''
