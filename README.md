# mule

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD013 -->

[![pipeline status](https://gitlab.com/yellowhat/mule/badges/main/pipeline.svg)](https://gitlab.com/yellowhat/mule/-/commits/main)

<!-- markdownlint-enable MD013 -->
<!-- markdownlint-restore -->

A self-hosted kubernetes cluster managed using:

* [Argo CD](https://argoproj.github.io/cd)
* [MetalLB](https://metallb.org/)
* [Renovate](https://renovatebot.com)
* [k6](https://k6.io)
* [Sealed Secrets](https://github.com/bitnami-labs/sealed-secrets)

## Available services

| Name     | IP              | Port  |
|----------|-----------------|-------|
| Grafana  | 192.168.178.200 | 80    |
| PiHole   | 192.168.178.201 | 53,80 |
| JellyFin | 192.168.178.202 | 80    |
| Argo CD  | NodePort        | 30080 |

## Bootstrap

1. Install `Fedora-Server-netinst-x86_64-37-1.7.iso`:

    * custom partition with `xfs`
    * minimal install
    * user `k8s` with password

2. Run the script [bootstrap/run.sh](bootstrap/run.sh) from another machine

## Hardware

* NiPoGi JK06 Mini PC (input 12V*2.5A)
* CPU: Intel Celeron N5100
  * Total Cores: 4
  * Total Threads: 4
  * Burst Frequency: 2.80 GHz
  * Base Frequency: 1.10 GHz
  * TDP 6 W
* RAM: 8 GB
* M.2 Sata: 256GB
* SSD Sata: Samsung SSD 860 EVO 1TB (S3YBNB0M304478P)
